# Running timescale on ubuntu with docker compose

## VMware
You have to set up the system to show WWN ids for disks if you use our ubuntu template, which doesn't enable them by default. Go to settings / vm options / edit configuration and add:

```
disk.EnableUUID TRUE
```

## Get started
```shell
git clone https://gitlab.oit.duke.edu/jnt6/timescale-ubuntu-deployment.git && cd timescale-ubuntu-deployment
cp -r docker-compose /srv/docker-compose
cp docker-compose@.service /etc/systemd/system/
echo 'docker-pgsql:x:70:70:Postgres user in docker container:/srv/postgresql_data:/usr/sbin/nologin' >> /etc/passwd
apt-get install -y zfsutils-linux zfs-auto-snapshot certbot
# Yo you should figure this one out
zpool create -o ashift=12 tank /dev/disk/by-id/$ZFS_BLOCK_DEVICE
zfs set compression=lz4 tank
zfs create tank/postgresql_data
zfs set com.sun:auto-snapshot=true tank
zfs set com.sun:auto-snapshot:daily=true tank
zfs set com.sun:auto-snapshot:monthly=false tank
zfs set com.sun:auto-snapshot:weekly=true tank
zfs set com.sun:auto-snapshot:hourly=false tank
zfs set com.sun:auto-snapshot:frequent=false tank
chown -R docker-pgsql /tank/*
systemctl enable --now docker-compose@timescale
# Let it create the data directory, then copy in the config file
cp postgresql.conf /tank/postgresql_data
# Shell script that helps you back things up
cp -p backup.sh /tank/postgresql_data/
systemctl restart docker-compose@timescale
```

## You're in the container's world now!

```shell
cd /srv/docker-compose/timescale && docker-compose exec timescale bash
# I'M IN A CONTAINER YAY
# Do the postgres user's password first
psql -U postgres # now type \password and put in a password! then back to the shell
# Make grafana user
echo 'CREATE ROLE "grafana"; ALTER ROLE "grafana" WITH LOGIN; ALTER user grafana with password '\''coolpassword'\'';' | psql -Upostgres
```

Read only grafana perms look like:
```
psql -U mycooldb
GRANT SELECT ON ALL TABLES IN SCHEMA public TO grafana;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO grafana;
```

If you are loading an existing dataset from another timescale db, use the restoring=on flag during your load

```shell
# Make database
echo "CREATE DATABASE perfdump; ALTER DATABASE perfdump SET timescaledb.restoring='on';" | psql -Upostgres
nohup pg_restore -U postgres -Fc -d perfdump /tank/db_backups/perfdump.bak
echo "ALTER DATABASE perfdump SET timescaledb.restoring='off';" | psql -Upostgres
```

## Backups!

Run

```
/tank/postgresql_data/backup.sh
```

But you should tweak the paths...

## TSM

exclude /tank!!!

## SSL

```shell
iptables -I INPUT -m tcp -p tcp --dport 80 -j ACCEPT
certbot --server https://locksmith.oit.duke.edu/acme/v2/directory certonly
cp /etc/letsencrypt/live/*/cert.pem /tank/postgresql_data/server.crt
cp /etc/letsencrypt/live/*/privkey.pem /tank/postgresql_data/server.key
chown docker-pgsql /tank/postgresql_data/server.*
chmod 600 /tank/postgresql_data/server.key
echo 'ssl = on' >> /tank/postgresql_data/postgresql.conf
systemctl restart docker-compose@timescale
```
